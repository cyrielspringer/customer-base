import rootReducer from '../reducers';
import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import promise from 'redux-promise-middleware';

const middleware = applyMiddleware(promise(), thunk, logger());

export default (initialState) => {
  return createStore(rootReducer, middleware, initialState);
};

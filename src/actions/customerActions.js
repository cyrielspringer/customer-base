import * as types from './actionTypes';
import Database from '../components/Database';

export const addCustomer = (customer) => {
  return {
    type: types.ADD_CUSTOMER,
    customer
  };
}

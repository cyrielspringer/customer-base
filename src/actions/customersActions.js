import * as types from './actionTypes';
import Database from '../components/Database';

export function fetchCustomers(limit, skip) {
  return function(dispatch) {
    dispatch({type: types.FETCH_CUSTOMERS_PENDING});

    const db = new Database().connect();
    let collection = db.collection('users');
    collection.all({limit: limit, skip: skip})
      .then((cursor) => { return cursor.all() })
      .then((values) => {
          dispatch({type: types.FETCH_CUSTOMERS_FULFILLED, payload: values});
      });
  }
}

export function fetchCustomersCount(pageLimit) {
  return function(dispatch) {
    dispatch({type: types.FETCH_CUSTOMERS_COUNT_PENDING});

    const db = new Database().connect();
    let collection = db.collection('users');
    collection.count().then(data => {
      dispatch({
          type: types.FETCH_CUSTOMERS_COUNT_FULFILLED, 
          payload: {
            count: data.count,
            pages: pageLimit !== undefined ? data.count / pageLimit : 1
          }
        });
    });
  }
}

export function changePage(newPage) {
  return {
    type: types.CHANGE_CUSTOMERS_PAGE,
    payload: newPage
  }
}

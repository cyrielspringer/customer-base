import * as types from '../actions/actionTypes';

const initialState = {
    customers: [],
    customersCount: 0,
    pages: null,
    pageLimit: 20,
    currentPage: 1,
    fetching: false,
    fetched: false,
    error: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_CUSTOMERS_PENDING: {
        return {
            ...state,
            fetching: true
        }
    }
    case types.FETCH_CUSTOMERS_REJECTED: {
        return {
            ...state,
            fetching: false,
            error: action.payload
        }
    }
    case types.FETCH_CUSTOMERS_FULFILLED: {
        return {
            ...state,
            fetching: false,
            fetched: true,
            customers: action.payload
        }
    }
    case types.FETCH_CUSTOMERS_COUNT_PENDING: {
        return {
            ...state
        }
    }
    case types.FETCH_CUSTOMERS_COUNT_REJECTED: {
        return {
            ...state,
            error: action.payload
        }
    }
    case types.FETCH_CUSTOMERS_COUNT_FULFILLED: {
        return {
            ...state,
            customersCount: action.payload.count,
            pages: action.payload.pages
        }
    }
    case types.CHANGE_CUSTOMERS_PAGE: {
        return {
            ...state,
            currentPage: action.payload
        }
    }
    default: 
        return state;
  }
};

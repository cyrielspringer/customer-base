import people from './peopleReducer.js';
import customer from './customerReducer';
import customers from './customersReducer';
import {combineReducers} from 'redux';

const rootReducer = combineReducers({
  people,
  customer,
  customers
});

export default rootReducer;

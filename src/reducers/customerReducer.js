import * as types from '../actions/actionTypes';



const initialState = {
    
}

export default (state = initialState, action) => {
  switch (action.type) {
    case types.ADD_CUSTOMER: {
        return {
            ...state
        }
    }
    case types.REMOVE_CUSTOMER: {
        return {
            ...state
        }
    }
    case types.UPDATE_CUSTOMER: {
        return {
            ...state
        }
    }
    case types.FETCH_CUSTOMER_PENDING: {
        return {
            ...state
        }
    }
    case types.FETCH_CUSTOMER_REJECTED: {
        return {
            ...state
        }
    }
    case types.FETCH_CUSTOMER_FULFILLED: {
        return {
            ...state
        }
    }
    default: 
        return state;
  }
};

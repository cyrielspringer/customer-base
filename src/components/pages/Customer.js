import React from 'react';
import Page from '../Page';
import Database from '../Database';
import CustomerView from '../customer/CustomerView';

class Customer extends Page {

  constructor(props) {
    super(props);
    this.db = new Database().connect();
  }

  componentWillMount() {
    let collection = this.db.collection('users');
    collection.document(this.props.params.id).then(
      doc => {
        this.user = doc;
        this.setState({
          title: `${this.user.name.first} ${this.user.name.last}`,
          user: this.user
        });
      },
      err => {
        this.setState({
          title: `Cannot find`,
          user: null
        });
      }
    );
    this.title = 'test'; 
  }

  renderContent() {
    return (
        <div>
          {this.state.user && <CustomerView user={this.state.user} />}
        </div>
    );
  }

}

export default Customer;

import React from 'react';
import Page from '../Page';
import CustomerTable from '../customer/CustomerTable';
import { Segment } from 'semantic-ui-react';

export default class Customers extends Page {
  
  componentWillMount() {
    this.setState({
      title: 'Customers'
    });
  }
  
  renderContent() {
    return (
      <Segment basic>
        {this.props.children ? this.props.children : <CustomerTable/> }
      </Segment>
    );
  }
}

import React, { Component } from 'react';
import { Menu } from 'semantic-ui-react'
import Dashboard from './Dashboard';

class App extends Component {

  static contextTypes = {
      router: React.PropTypes.object.isRequired
  };

  constructor(props, context) {
    super(props, context);
    this.activeItem = '';
  }

  cloneChildren() {
    var path = this.props.location.pathname;
    if (this.props.children) {
      return React.cloneElement(this.props.children, { key: path })
    }
    else {
      return (
        <Dashboard key={path}/>
      )
    }
  }

  handleItemClick(event, name) {
      if(name === 'home') name = '';
      let route = `/${name}`;
      this.activeItem = route;
      this.context.router.push(route);
    }

  isActive(name) {
      if(name === 'home') name = '';
      try {
        let paths = this.props.location.pathname.split('/');
        let currentPath = '';
        if(paths.length > 1) {
          currentPath = paths[1];
        }
        if(name === currentPath.toLowerCase()) {
          return true;
        }
        return false;
      }
      catch(e) {
        console.log(e);
        return false;
      }
    }

  render() {
    let menuWidth = 250;
    return (
      <div>
        <Menu pointing inverted vertical style={{position: 'fixed', top: 0, left: 0, width: menuWidth, borderRadius: 0, height: '100%'}}>
          <Menu.Item>
            <Menu.Header>Surfinno backend</Menu.Header>
          </Menu.Item>
          <Menu.Item>
            <Menu.Header>Home</Menu.Header>
            <Menu.Menu>
              <Menu.Item name='dashboard' active={this.isActive({name})} onClick={(e, {name}) => {this.handleItemClick(e, name)}} />
              <Menu.Item name='customers' active={this.isActive({name})} onClick={(e, {name}) => {this.handleItemClick(e, name)}} />
            </Menu.Menu>
          </Menu.Item>
        </Menu>

        <div className="app content" style={{marginLeft: menuWidth, padding: 30, maxWidth: 1200}}>
        {this.cloneChildren()}
        </div>
      </div>
    );
  }
}

export default App;

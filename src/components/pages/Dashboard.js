import Page from '../Page';

class Dashboard extends Page {

  constructor(props) {
    super(props);
    this.title = 'Dashboard';
  }

}

export default Dashboard;

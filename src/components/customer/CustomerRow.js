import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';

export default class CustomerRow extends Component {

  onClickCallback = (userID) => (event) => {
    event.preventDefault();
    if (typeof(this.props.onUserClicked) !== "undefined" &&
        typeof(this.props.onUserClicked) === "function") {
      this.props.onUserClicked({userID});
    }
  }

  render() {
    return (
        <Table.Row onClick={this.onClickCallback(this.props.user._key)}>
          <Table.Cell collapsing>{this.props.user.name.first}</Table.Cell>
          <Table.Cell collapsing>{this.props.user.name.last}</Table.Cell>
          <Table.Cell>{this.props.user.contact.email.join(', ')}</Table.Cell>
          <Table.Cell>{this.props.user.birthday}</Table.Cell>
          <Table.Cell>{this.props.user.gender}</Table.Cell>
          <Table.Cell>{this.props.user.memberSince}</Table.Cell>
        </Table.Row>
    );
  }
}

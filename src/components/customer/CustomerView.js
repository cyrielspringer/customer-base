import React, { Component } from 'react';
import { Segment, Grid, Image, Form, Button, Icon, Divider} from 'semantic-ui-react';
import EditableFormInput from '../form/EditableFormInput';
import EditableFormSelect from '../form/EditableFormSelect';

export default class CustomerView extends Component {

  static defaultProps = {
    canEdit: true
  }

  constructor(props) {
    super(props);
    this.state = {
      canEdit: this.props.canEdit
    }
    this.genderOptions = [
      { key: 'm', text: 'Male', value: 'male' },
      { key: 'f', text: 'Female', value: 'female' },
    ]
  }

  componentWillMount() {
    
  }
  
  toggleEditing = (event) => {
    this.setState((prevState, props) => ({
      canEdit: !prevState.canEdit
    }));
  }

  render() {
    let user = this.props.user;
    return (
        <Segment basic>
          <Grid >
            <Grid.Row>
              {/* Client information */}
              <Grid.Column width={12}>
                <Segment basic>
                  <Form>
                    <Form.Group widths='equal'>
                      <EditableFormInput canEdit={this.state.canEdit} label='First name' placeholder='First name' value={user.name.first} />
                      <EditableFormInput canEdit={this.state.canEdit} label='Last name' placeholder={this.label} value={user.name.last} />
                    </Form.Group>
                    <Form.Group widths='equal'>
                      <EditableFormSelect canEdit={this.state.canEdit} label='Gender' options={this.genderOptions} placeholder={this.label} value={this.props.user.gender} />
                      <EditableFormInput canEdit={this.state.canEdit} label='Birthday' placeholder={this.label} value={this.props.user.birthday} />
                    </Form.Group>
                    <EditableFormInput canEdit={this.state.canEdit} label='Email' placeholder={this.label} value={this.props.user.contact.email.join(', ')} />
                    <EditableFormInput canEdit={this.state.canEdit} label='Registered on' placeholder={this.label} value={this.props.user.memberSince} />
                  </Form>
                  <Divider hidden/>
                  <Button primary onClick={this.toggleEditing}><Icon name='edit' />{this.state.canEdit ? 'Disable editing' : 'Enable editing' }</Button>
                </Segment>
              </Grid.Column>
              <Grid.Column width={4}>
                <Segment>
                  <Image src='http://semantic-ui.com/images/avatar2/large/matthew.png' />
                </Segment>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
    );
  }
}

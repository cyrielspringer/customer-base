import React, { Component } from 'react';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Table, Dimmer, Loader} from 'semantic-ui-react';

import * as customersActions from '../../actions/customersActions';
import CustomerRow from './CustomerRow';
import Pagination from '../pagination/Pagination';

function mapStateToProps(store, props) {
  const {customers} = store;
  return {...customers};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(customersActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  
  class CustomerTable extends Component {

    static contextTypes = {
        router: React.PropTypes.object.isRequired
    };

    componentDidMount() {
      this.loadCustomers();
    }

    componentWillUnmount() {
    }

    loadCustomers() {
      this.props.actions.fetchCustomersCount(this.props.pageLimit);
      this.props.actions.fetchCustomers(this.props.pageLimit, (this.props.currentPage - 1) * this.props.pageLimit);
    }

    onUserClicked = (user) => {
      let userID = user.userID;
      this.context.router.push(`customers/${userID}`);
    }

    renderUsers() {
      if(this.props.customers) {
        return this.props.customers.map((user) => {
          return (
            <CustomerRow key={user._key} user={user} onUserClicked={this.onUserClicked}/>
          )
        })
      }
      return null;
    }

    updatePagination = (currentPage) => {
      this.props.actions.changePage(currentPage);
      this.loadCustomers();
    }

    render() {
      console.log(this.props);
      return (
        <div>
        {this.props.fetching &&
          <Dimmer active>
          <Loader>Loading</Loader>
          </Dimmer>
        }
          <Table compact='very' celled singleLine selectable>

            <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Firstname</Table.HeaderCell>
              <Table.HeaderCell>Lastname</Table.HeaderCell>
              <Table.HeaderCell>E-mail address</Table.HeaderCell>
              <Table.HeaderCell>Birthdate</Table.HeaderCell>
              <Table.HeaderCell>Gender</Table.HeaderCell>
              <Table.HeaderCell>Registerdate</Table.HeaderCell>
            </Table.Row>
            </Table.Header>

            <Table.Body>
              {this.renderUsers()}
            </Table.Body>
            <Table.Footer>

            <Table.Row>
              <Table.HeaderCell colSpan='6'>
                <Pagination updatePagination={this.updatePagination} currentPage={this.props.currentPage} totalPages={this.props.pages}/>
              </Table.HeaderCell>
            </Table.Row>
            </Table.Footer>
          </Table>
          Customers: {this.props.customersCount}
        </div>
      );
    }
  }
);
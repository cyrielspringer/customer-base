import React, { Component } from 'react';
import {Form} from 'semantic-ui-react';

export default class EditableFormInput extends Component {

    static defaultProps = {
        fallbackPlaceholder: '',
        canEdit: false
    }

    constructor(props, context) {
        super(props, context);
        console.log(props.canEdit);
        this.state = {
            value: props.value
        }
    }
    
    onChangeCallback = (event) => {
        this.setState({
            value: event.target.value
        });
        if (typeof(this.props.onChange) !== "undefined" &&
            typeof(this.props.onChange) === "function") {
            this.props.onChange(event);
        }
        event.preventDefault();
    }

    getCleanProps() {
        let newProps = Object.assign({}, this.props);
        delete newProps.fallbackPlaceholder;
        delete newProps.canEdit;
        delete newProps.value;
        return newProps;
    }

    render() {
        console.log(this.props.canEdit);
        if(this.props.canEdit) {
            return (
                <Form.Input {...this.getCleanProps()} value={this.state.value} onChange={this.onChangeCallback} />
            );
        }
        else {
            return (
            <div className='field'>
                {this.props.label && <label>{this.props.label}</label>}
                {this.state.value && <div>{this.state.value}</div>}
            </div>
            );
        }
    }
}
import React, { Component } from 'react';
import { Header, Grid, Image } from 'semantic-ui-react'


export default class Page extends Component {

  constructor(props) {
    super(props);
    this.state = {
      title: ''
    }
  }

  renderTitle() {
    return (
      <div>
        <Header as='h1'>{this.state.title}</Header>
      </div>
    );
  }

  renderContent() {
    return (
      <Grid.Row columns={3}>
        <Grid.Column>
          <Image src='http://semantic-ui.com/images/wireframe/paragraph.png' />
        </Grid.Column>
        <Grid.Column>
          <Image src='http://semantic-ui.com/images/wireframe/paragraph.png' />
        </Grid.Column>
        <Grid.Column>
          <Image src='http://semantic-ui.com/images/wireframe/paragraph.png' />
        </Grid.Column>
      </Grid.Row>
    )
  }

  render() {
    return (
      <Grid divided='vertically' >
        <Grid.Row>
          <Grid.Column>
            {this.renderTitle()}
          </Grid.Column>
        </Grid.Row>

        {this.renderContent()}
      </Grid>
    );
  }
}

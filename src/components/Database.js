import arangojs, {aql} from 'arangojs';


class Database {

  connect() {
    // let host = process.env.ARANGODB_HOST || 'db1.surfinno.dev.adriaanse.it';
    // let port = process.env.ARANGODB_PORT || '8529';
    // let database = process.env.ARANGODB_DB || 'dev-backend';
    // let username = process.env.ARANGODB_USERNAME || 'roy';
    // let password = process.env.ARANGODB_PASSWORD || 'tvYLuQczEaDOJ6rnNNFWuCAqd';
    let host = process.env.ARANGODB_HOST || '172.17.0.2';
    let port = process.env.ARANGODB_PORT || '8529';
    let database = process.env.ARANGODB_DB || '_system';
    let username = process.env.ARANGODB_USERNAME || 'root';
    let password = process.env.ARANGODB_PASSWORD || 'xXoqmW2QfZu6ELsO';
    return arangojs({
      url: `http://${username}:${password}@${host}:${port}`,
      databaseName: database
    });
  }

  query(...args) {
    let db = this.connect();
    db.query(args);
  }

  aqlQuery(arg) {
    let db = this.connect();
    db.query(aql`${arg}`);
  }
}

export default Database;

import React, { Component } from 'react';
import { Menu } from 'semantic-ui-react';

export default class PageView extends Component {

  static defaultProps = {
    active: false,
    page: ''
  }

  render() {
    let onClick = this.props.onClick;
    return (
      <Menu.Item
        as='a'
        active={this.props.active}
        onClick={onClick}
        onKeyPress={onClick}
        tabIndex="0">
        {this.props.page}
        {this.props.children}
      </Menu.Item>
    );
  }
}

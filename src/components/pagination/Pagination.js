import React, { Component } from 'react';
import { Menu, Icon } from 'semantic-ui-react';
import PageView from './PageView';

export default class Pagination extends Component {

  static defaultProps = {
    totalPages: 1,
    currentPage: 1,
    maxPagesVisible: 15
  };

  handlePageClick = (page) => (event) => {
    event.preventDefault();
    let newPage = parseInt(page, 10);
    if(newPage === this.props.currentPage) return;
    if(newPage < 1) return;
    if(newPage > this.props.totalPages) return;
    this.props.updatePagination(newPage);
  }


  componentDidMount() {
  }

  componentWillUnmount() {
  }

  render() {
    let pageLinks = [];
    let maxPageVisible = this.props.maxPagesVisible;
    let totalPages = this.props.totalPages;
    let addExtraMenuItem = false;
    let addExtraMenuItemStart = false;
    let startItem = 1;
    let pageLoop = totalPages > maxPageVisible ? maxPageVisible : totalPages;

    if(totalPages > maxPageVisible) {
      addExtraMenuItem = true;
    }

    if(this.props.currentPage > 5) {
      startItem = this.props.currentPage - 4;
      addExtraMenuItemStart = true;
      if(startItem > totalPages - maxPageVisible) {
        startItem = (totalPages + 1) - maxPageVisible;
        addExtraMenuItem = false;
      }
    }

    let i = startItem;
    if(addExtraMenuItemStart) {
      pageLinks.push(
        <PageView
          key={'startPageItem'}
          page='...'
          onClick={this.handlePageClick(i - 1)} />
      );
    }
    for(i = startItem; i < startItem + pageLoop; i++) {
      pageLinks.push(
        <PageView
          key={ i + 'PageItem'}
          page={i}
          onClick={this.handlePageClick(i)}
          active={this.props.currentPage === i}
        />
      );
    }
    if(addExtraMenuItem) {
      pageLinks.push(
        <PageView
          key={'endPageItem'}
          page='...'
          onClick={this.handlePageClick(i + 1)} />
      );
    }

    return (
      <Menu pagination>
        <PageView
          onClick={this.handlePageClick(this.props.currentPage - 1)} >
          <Icon name='left chevron' />
          Prev
        </PageView>
        {pageLinks}
        <PageView
          onClick={this.handlePageClick(this.props.currentPage + 1)} >
          Next
          <Icon name='right chevron' />
        </PageView>
      </Menu>
    )
  }
}

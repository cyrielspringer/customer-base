import React, { PropTypes } from 'react';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory, IndexRoute } from 'react-router'
import App from './pages/App';
import Dashboard from './pages/Dashboard';
import Customers from './pages/Customers';
import Customer from './pages/Customer';

const Root = ({ store }) => (
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={App}>
        <IndexRoute component={Dashboard}/>
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/customers" component={Customers}>
          <Route path="/customers/:id" component={Customer} />
        </Route>
      </Route>
    </Router>
  </Provider>
);

Root.propTypes = {
  store: PropTypes.object.isRequired,
};

export default Root;